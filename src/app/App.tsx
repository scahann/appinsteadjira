import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Component } from './react/component';
declare let module: any

ReactDOM.render(
    <Component />,
  document.getElementById('root')
);


if (module.hot) {
   module.hot.accept();
}

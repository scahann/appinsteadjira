import { JsonObject, JsonProperty } from "json2typescript";


@JsonObject("Assignee")
export class Assignee {
  @JsonProperty("display_name", String)
  public nick: string = undefined;
}

@JsonObject("Author")
export class Author {
  @JsonProperty("raw", String)
  public raw: string = undefined;
}

@JsonObject("Issue")
export class Issue {
  @JsonProperty("priority", String)
  public priority: string = undefined;
  @JsonProperty("kind", String)
  public kind: string = undefined;
  @JsonProperty("title", String)
  public title: string = undefined;
  @JsonProperty("created_on", String)
  public created_on: string = undefined;
  @JsonProperty("updated_on", String)
  public updated_on: string = undefined;
  @JsonProperty("id", Number)
  public id: number = undefined;
  @JsonProperty("assignee", Assignee, true)
  public assignee: Assignee = undefined;
  @JsonProperty("state", String)
  public state: string = undefined;
}

@JsonObject("Commit")
export class Commit {
  @JsonProperty("date", String)
  public date: string = undefined;
  @JsonProperty("message", String)
  public message: string = undefined;
  @JsonProperty("author", Author)
  public author: Author = undefined;
}

export interface UssuesCommits {
  commits: Array<Commit>;
  issues: Array<Issue>;
}

import { Issue, Commit } from "./data";
import axios, { AxiosResponse } from "axios";
import { JsonConvert } from "json2typescript/src/json2typescript/json-convert";

export function getIsuesAndCommits (projectName: string, userName: string,
  onSuccess: (workers: Array<string>,
    ussues: Map<string, Array<Issue>>,
    commits: Map<string, Array<Commit>>, freeIssues?: Array<Issue>) => void,
  errorCallback: (error: void) => void) {

    let jsonConvert: JsonConvert = new JsonConvert();
  let commitsPromise = axios({
    method: "GET",
    url: 'https://api.bitbucket.org/2.0/repositories/'
      .concat(userName)
      .concat('/').concat(projectName)
      .concat('/commits'),
    });
  var issuesPromise = axios({
    method: "GET",
    url: 'https://api.bitbucket.org/2.0/repositories/'
      .concat(userName)
      .concat('/').concat(projectName)
      .concat('/issues')
    });

    Promise.all([
      commitsPromise,
      issuesPromise,
    ]).then((values: Array<AxiosResponse>) => {

      let arrayCommits: Array<Commit> = values[0].data.values.map((item: Commit) => {
        return jsonConvert.deserializeObject(item, Commit);
      });
      let arrayIssues: Array<Issue> = values[1].data.values.map((item: Issue) => {
        return jsonConvert.deserializeObject(item, Issue);
      });
      let data: Data = getWorkersWithCommitsAndIssues(arrayCommits, arrayIssues);
      onSuccess(data.workers, data.issues, data.commits, data.freeIssues);

    }).catch((error: any) => {
      errorCallback(error);
    });

}

function getWorkersWithCommitsAndIssues(commits: Array<Commit>, issues: Array<Issue>){

  let commitsMap: Map<string, Array<Commit>> = new Map<string, Array<Commit>>();
  let issuesMap: Map<string, Array<Issue>> = new Map<string, Array<Issue>>();
  let freeIssues: Array<Issue> = [];
  let nickWithEmails: Array<string> = [];

  commits.forEach((commit: Commit) => {
    if (commitsMap.has(commit.author.raw)) {
      commitsMap.get(commit.author.raw).push(commit);
    } else {
      commitsMap.set(commit.author.raw, [commit]);
      nickWithEmails.push(commit.author.raw);
    }
  });

  issues.forEach((issue: Issue) => {
    if (issue.assignee == null) {
      freeIssues.push(issue);
    } else {
      let nickWithEmail: string = nickInArray(issue.assignee.nick, nickWithEmails);
      if (nickWithEmail) {
        if (issuesMap.has(nickWithEmail)) {
          issuesMap.get(nickWithEmail).push(issue);
        } else {
          issuesMap.set(nickWithEmail, [issue])
        }
      } else {
        issuesMap.set(nickWithEmail, [issue]);
        nickWithEmails.push(nickWithEmail);
      }
    }
  });
  return {issues: issuesMap, commits: commitsMap, workers: nickWithEmails, freeIssues: freeIssues};
}


interface Data {
  workers: Array<string>;
  issues: Map<string, Array<Issue>>;
  commits: Map<string, Array<Commit>>;
  freeIssues?: Array<Issue>;
}

function nickInArray(nick: string, nickWithEmails: Array<string>): string {
  return nickWithEmails.find((nickWithEmail: string) => {
    if (nickWithEmail.includes(nick)) return nickWithEmail;
  });
}

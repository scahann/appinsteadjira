import * as React from 'react';
import { Commit, Issue } from '../data';
import { getIsuesAndCommits } from '../API';
import "../styles.css";

interface ComponentState {
  projectName: string;
  userName: string;
  workers: Array<string>;
  issues: Map<string, Array<Issue>>;
    freeIssues?: Array<Issue>;
  commits: Map<string, Array<Commit>>;
  error?: any;
  loading?: boolean;
}


export class Component extends React.Component<{}, ComponentState> {
  constructor(props: any) {
    super(props);
    this.state = {
      projectName: "appinsteadjira",
      userName: "scahann",
      workers: [],
      issues: new Map<string, Array<Issue>>(),
      commits: new Map<string, Array<Commit>>(),
      freeIssues: []
    };
  }

  onChangeProjectName = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      projectName: event.target.value
    });
  };

  onChangeUserName = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      userName: event.target.value
    });
  };

  onClickGet = () => {
    if (this.state.projectName != '' && this.state.userName != '') {
      getIsuesAndCommits(this.state.projectName, this.state.userName, this.onSuccess, this.onError)
      this.setState({
        loading: true
      })
    }
  };

  onError = (error: any) => {
    this.setState({error: error, loading: false})

  }

  onSuccess = (  workers: Array<string>,
    issues: Map<string, Array<Issue>>,
    commits: Map<string, Array<Commit>>,freeIssues?: Array<Issue>) => {
      console.log(freeIssues)
    this.setState({
      workers: workers,
      issues: issues,
      commits: commits,
      loading: false,
      freeIssues: freeIssues
    })
  }

  render() {
    return (
      <div className="main">
        <div className="guide">Enter your username and project name to view commits (repository should
          have public access)</div>
        <input
        className="input"
        type = "text"
        placeholder = "project name"
        value = { this.state.projectName }
        onChange = { this.onChangeProjectName }
        />
        <input
        className="input"
        type = "text"
        placeholder = "user name"
        value = { this.state.userName }
        onChange = { this.onChangeUserName } />
        <button className="button" onClick={ this.onClickGet }> Get commits </button>
        {this.state.loading && <div className="loading">Loading...</div>}
        {this.state.error && <div className="error">{this.state.error}</div>}
        <div className="results">



        {this.state.freeIssues.length != 0 &&
          <div className="worker">
          <div className="workerName">Free issues</div>
          <div className="infoContainer">
          <div className="issues">
            {this.state.freeIssues.map((issue: Issue) =>
              <div className="issue">
                <div className="issueName">{'Issue#'.concat(issue.id.toString()).concat(" ").concat(issue.title)}</div>
                <div >{"Kind: "} <div className="kind" style={getColor(issue.kind)}>{issue.kind}</div></div>
                <div>{"Priority: "} <div className="priority" style={getColor(issue.priority)}>{issue.priority}</div></div>
                <div>{"State: "} <div className="issueState" style={getColor(issue.state)}>{issue.state}</div></div>
                <div className="time">{'Created on '.concat(issue.created_on.slice(0, 16).replace('T', ' '))}</div>
                <div className="time">{'Updated on '.concat(issue.updated_on.slice(0, 16).replace('T', ' '))}</div>
              </div>
            )
          }
          </div
          ></div>
          </div>
        }

        {this.state.workers.map((worker: string) =>
          <div className="worker" key={worker}>
            <div className="workerName">{worker}</div>
            <div className="infoContainer">

            {
              this.state.issues.has(worker) &&
              <div className="issues">
              <p className="infoHeader">Issues:</p>
              {this.state.issues.get(worker).map((issue: Issue, index: number) =>
                <div className="issue" key={index}>
                  <div className="issueName">{'Issue#'.concat(issue.id.toString()).concat(" ").concat(issue.title)}</div>
                  <div >{"Kind: "} <div className="kind" style={getColor(issue.kind)}>{issue.kind}</div></div>
                  <div>{"Priority: "} <div className="priority" style={getColor(issue.priority)}>{issue.priority}</div></div>
                  <div>{"State: "} <div className="issueState" style={getColor(issue.state)}>{issue.state}</div></div>
                  <div className="time">{'Created on '.concat(issue.created_on.slice(0, 16).replace('T', ' '))}</div>
                  <div className="time">{'Updated on '.concat(issue.updated_on.slice(0, 16).replace('T', ' '))}</div>

                </div>
              )}
              </div>
            }

            {this.state.commits.has(worker) &&
              <div className="commits">
              <p className="infoHeader">Commits:</p>
              {this.state.commits.get(worker).map((commit: Commit, index: number) =>
                <div className="commit" key={index}>
                <div className="commitName">{commit.message}</div>
                <div className="commitDate">{commit.date.slice(0, 16).replace('T', ' ')}</div>
                </div>
              )}
              </div>
            }


            </div>

          </div>
        )
        }
        </div>
      </div>
    )
  };
}

function getColor(title: string): React.CSSProperties {
  let color: string = "#000000";
  if (title == "major" || title == "critical" || title == "blocker") {
    color = "#CB4949";
  } else if (title == "new") {
    color = "#146C57";
  } else if (title == "resolved") {
    color = "#004BAD";
  }
  const style: React.CSSProperties = {
    'color': color
  };
  return style;
}
